from s_a import shortest_path_search

def test_shortest_path_correctness():
    test_graph = {
        'A': ['B', 'E', 'C'],
        'B': ['A', 'D', 'E'],
        'C': ['A', 'F', 'G'],
        'D': ['B', 'E'],
        'E': ['A', 'B', 'D'],
        'F': ['C', 'I'],
        'G': ['C', 'H'],
        'H': ['G', 'I', 'J'],
        'I': ['H', 'J', 'F'],
        'J': ['H', 'I']
    }
    shortest_path, operations = shortest_path_search(test_graph, 'A', 'J')
    assert shortest_path == ['A', 'C', 'F', 'I', 'J']
    assert operations == 110
