from collections import defaultdict

def construct_graph(edge_list):
    graph_structure = defaultdict(list)
    for edge in edge_list:
        node1, node2 = edge
        graph_structure[node1].append(node2)
        graph_structure[node2].append(node1)
    return graph_structure

def shortest_path_search(graph, source, target):
    ops_counter = 0
    visited_nodes = []
    paths_queue = [[source]]
    if source == target:
        ops_counter += 1
        return None, ops_counter

    while paths_queue:
        current_path = paths_queue.pop(0)
        current_node = current_path[-1]
        ops_counter += 2

        if current_node not in visited_nodes:
            neighbors = graph[current_node]
            ops_counter += 1

            for neighbor in neighbors:
                ops_counter += 3
                updated_path = list(current_path)
                updated_path.append(neighbor)
                paths_queue.append(updated_path)

                if neighbor == target:
                    ops_counter += 1
                    return updated_path, ops_counter
            visited_nodes.append(current_node)
            ops_counter += 1
    return None, ops_counter
