import matplotlib.pyplot as plt
import networkx as nx
from s_a import shortest_path_search

def generate_graph(num_nodes, edge_probability):
    return nx.gnp_random_graph(num_nodes, edge_probability)

operations_vs_sizes = []
graph_sizes = list(range(10, 110, 10))

for size in graph_sizes:
    random_graph = generate_graph(size, 0.8)
    _, operation_count = shortest_path_search(random_graph, 0, size - 1)
    operations_vs_sizes.append(operation_count)

plt.plot(graph_sizes, operations_vs_sizes)
plt.xlabel('Размерность графа')
plt.ylabel('Количество операций')
plt.title('Сложность поиска кратчайшего пути в графе')
plt.show()